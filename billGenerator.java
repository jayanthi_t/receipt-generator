package com.company;
import java.util.Scanner;

public class billGenerator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the Number of Products: ");
        int numOfProducts = Integer.parseInt(input.nextLine());

        String[] productList = new String[numOfProducts];

        for(int i=0;i<numOfProducts;i++) {
            System.out.println("Enter Product "+i+" List in the format of Product_Type:Quantity:Unit Price : ");
            productList[i] = input.nextLine();
        }
        PrintReceipt(numOfProducts,productList);
    }

    public static void PrintReceipt(int numOfProducts,String[] productList){

        double total_tax=0.0;
        double total_price=0.0;
        double taxPerProduct = 0.0;

        System.out.println("Billing..................:");
        for(int i=0;i<numOfProducts;i++) {
            String[] product = productList[i].split(":");

            String productType = product[0];
            int quantity = Integer.parseInt(product[1]);
            double unitPrice = Double.parseDouble(product[2]);

            if (productType.contains("imported")) {
                if (productType.contains("book") || productType.contains("food") || productType.contains("medicine")) {
                    taxPerProduct = unitPrice * 0.05;
                    unitPrice = (unitPrice + taxPerProduct) * quantity;
                    total_tax += (taxPerProduct * quantity);
                    total_price += unitPrice;
                }
                else {
                    taxPerProduct = unitPrice * 0.15;
                    unitPrice = (unitPrice + taxPerProduct)*quantity;
                    total_tax += (taxPerProduct * quantity);
                    total_price += unitPrice;
                }
            }
            else {
                if (productType.contains("book") || productType.contains("food") || productType.contains("medicine")) {
                    unitPrice = (unitPrice * quantity);
                    total_price += unitPrice;
                }
                else {
                    taxPerProduct = unitPrice * 0.10;
                    unitPrice = (unitPrice + taxPerProduct)*quantity;
                    total_tax += (taxPerProduct * quantity);
                    total_price += unitPrice;
                }
            }
            System.out.println(productType + " : " + quantity + " : " + unitPrice+"\n");
        }
        System.out.println("TOTAL TAX: "+total_tax+"\n"+"TOTAL PRICE: "+total_price);
    }
}
